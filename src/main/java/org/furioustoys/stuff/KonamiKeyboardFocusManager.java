package org.furioustoys.stuff;

import org.furioustoys.stuff.utils.AudioStuff;
import org.furioustoys.stuff.utils.MyPrinter;

import javax.imageio.ImageIO;
import javax.sound.sampled.Clip;
import java.awt.KeyEventDispatcher;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class KonamiKeyboardFocusManager implements KeyEventDispatcher
{
    private static final int[] PATTERN = new int[]{KeyEvent.VK_UP,KeyEvent.VK_UP,KeyEvent.VK_DOWN,KeyEvent.VK_DOWN,KeyEvent.VK_LEFT,KeyEvent.VK_RIGHT,KeyEvent.VK_LEFT,KeyEvent.VK_RIGHT,KeyEvent.VK_B,KeyEvent.VK_A};
    private int pointer;
    private final Clip clip;
    private final FlashShower flashShower;

    public KonamiKeyboardFocusManager(String soundName, String pictureName) throws Exception
    {
        pointer = 0;
        try (InputStream inputStream = new BufferedInputStream(Files.newInputStream(Paths.get(soundName)))){
            this.clip = AudioStuff.createOutputClip(inputStream);
        }
        flashShower = new FlashShower(new FlashShower.FlashFrame(ImageIO.read(new File(pictureName))), 700);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent e)
    {
        if (e.getID() == KeyEvent.KEY_PRESSED)
        {
            if (e.getKeyCode() == PATTERN[pointer])
            {
                ++pointer;
                if (pointer >= PATTERN.length)
                {
                    pointer = 0;
                    clip.setMicrosecondPosition(0);
                    clip.start();
                    MyPrinter.print("DUPA");
                    synchronized (flashShower) {
                        flashShower.notify();
                    }
                }
            }
            else
                pointer = 0;
        }
        return false;
    }

    @Override
    protected void finalize() {
        clip.close();
        flashShower.interrupt();
    }

}
