package org.furioustoys.stuff;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Matrix {
    private static Thread worker = null;

    public static void doThings() {
        if (worker == null) {
            try {
                worker = new Thread(new Worker());
                worker.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static class Worker implements Runnable {

        public static final Point startPosition = new Point(10, 20);
        private Font font;

        public Worker() throws IOException, FontFormatException {
            this.font = Font.createFont(Font.TRUETYPE_FONT, new File(getClass().getClassLoader().getResource("matrix.ttf").getFile()));
            //this.font = new Font("Arial Black", Font.PLAIN, 20);
        }

        @Override
        public void run() {
            JFrame frame = new JFrame();
            frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
            frame.setUndecorated(true);
            frame.add(new MatrixPanel(font));
            KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(e -> {
                System.out.println(e.getKeyChar());
                System.exit(0);
                return false;
            });
            frame.pack();
            frame.setVisible(true);
        }
    }

    public static class MatrixPanel extends JPanel {

        private Font font;

        public MatrixPanel(Font font)
        {
            this.font = font;
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            setBackground(Color.BLACK);
            Graphics2D graphics2D = (Graphics2D) g;
            graphics2D.setFont(font);
            graphics2D.setColor(Color.GREEN);
            graphics2D.drawString("dupa xD", Worker.startPosition.x, Worker.startPosition.y);
        }
    }
}
