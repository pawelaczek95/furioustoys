package org.furioustoys.stuff;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

public class FlashShower extends Thread {

    private final FlashFrame flashFrame;
    private final int showingTime;

    public FlashShower(FlashFrame flashFrame, int showingTime) {
        this.flashFrame = flashFrame;
        this.showingTime = showingTime;
        start();
    }

    @Override
    public void run() {
        try {
            boolean isShowing = false;
            while (true) {
                if (!isShowing) {
                    synchronized (this) {
                        wait();
                    }
                    isShowing = true;
                    flashFrame.setVisible(true);
                } else {
                    Thread.sleep(showingTime);
                    isShowing = false;
                    flashFrame.setVisible(false);
                }
            }
        } catch (InterruptedException ignored) {
        }
    }

    public void show() {
        synchronized (this) {
            notify();
        }
    }

    public static class FlashFrame extends JFrame {

        private final BufferedImage picture;

        public FlashFrame(BufferedImage picture) throws HeadlessException {
            this.picture = picture;
            setLayout(new BorderLayout());
            resetLocation();
            add(new JLabel(new ImageIcon(picture.getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH))), BorderLayout.CENTER);
            setUndecorated(true);
            pack();
            repaint();
        }

        public void resetLocation() {
            Dimension resolution = Toolkit.getDefaultToolkit().getScreenSize();
            double widthCoefficient = ((double) resolution.width) / picture.getWidth();
            double heightCoefficient = ((double) resolution.height) / picture.getHeight();
            double coefficient = Math.min(widthCoefficient, heightCoefficient);
            coefficient = Math.min(coefficient, 1.5);

            setSize((int) (coefficient * picture.getWidth()), (int) (coefficient * picture.getHeight()));
            setLocation((resolution.width - getWidth()) / 2, (resolution.height - getHeight()) / 2);
        }

        @Override
        public void setVisible(boolean b) {
            setLocationByPlatform(b);
            setAlwaysOnTop(b);
            super.setVisible(b);
        }
    }
}
