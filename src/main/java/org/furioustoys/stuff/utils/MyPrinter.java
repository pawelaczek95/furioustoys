package org.furioustoys.stuff.utils;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyPrinter {
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS");
    public static void print(Object object) {
        print(object, System.out);
    }

    public static void printErr(Object object) {
        print(object, System.err);
    }

    public static void print(Object object, PrintStream printStream) {
        printStream.println("[" + Thread.currentThread().getName() + "] " + DATE_FORMAT.format(new Date()) + ": " + object);
    }
}
