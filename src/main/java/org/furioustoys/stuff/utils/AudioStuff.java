package org.furioustoys.stuff.utils;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.io.InputStream;

public class AudioStuff {

    private AudioStuff(){}

    public static TargetDataLine getRecordLine() throws LineUnavailableException {
        return getRecordLine(getDefaultAudioFormat());
    }

    public static TargetDataLine getRecordLine(AudioFormat format) throws LineUnavailableException {
        return getRecordLine(format, ((int) format.getSampleRate()));
    }
    public static TargetDataLine getRecordLine(AudioFormat format, int bufferSize) throws LineUnavailableException {
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format, bufferSize);

        if (!AudioSystem.isLineSupported(info)) {
            return null;
        }
        TargetDataLine line = (TargetDataLine) AudioSystem.getLine(info);
        line.open(format);
        return line;
    }

    public static AudioFormat getDefaultAudioFormat() {
        float sampleRate = 44100;
        int sampleSizeInBits = 16;
        int channels = 1;
        return new AudioFormat(sampleRate, sampleSizeInBits,
                channels, true, true);
    }

    public static SourceDataLine createOutputLine(AudioFormat decodedFormat) throws LineUnavailableException {
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, decodedFormat);
        SourceDataLine outputLine = (SourceDataLine) AudioSystem.getLine(info);
        outputLine.open(decodedFormat);
        return outputLine;
    }

    public static Clip createOutputClip(InputStream inputStream) throws LineUnavailableException, UnsupportedAudioFileException, IOException {
        try (AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(inputStream)) {
            Clip clip = (Clip) AudioSystem.getLine(new DataLine.Info(Clip.class, audioInputStream.getFormat()));
            clip.open(audioInputStream);
            return clip;
        }
    }

    public static void playDummySound(String soundName) {
        try (InputStream soundStream = Utils.class.getClassLoader().getResourceAsStream(soundName)) {
            if (soundStream != null) {
                try (Clip clip = createOutputClip(soundStream)) {
                    clip.start();
                    while (!clip.isActive())
                        Thread.sleep(10);
                    while (clip.isActive()) {
                        Thread.sleep(10);
                    }
                }
            }
        } catch (Exception e) {
            MyPrinter.printErr("Error while playing sound");
            e.printStackTrace();
        }
    }
}
