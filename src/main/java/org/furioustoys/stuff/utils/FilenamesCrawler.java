package org.furioustoys.stuff.utils;

import java.io.File;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class FilenamesCrawler implements Iterable<File> {
    private final File[] root;

    public FilenamesCrawler(String root) {
        this(new File(root));
    }
    public FilenamesCrawler(File root) {
        this(root.listFiles());
    }
    public FilenamesCrawler(File[] rootList) {
        this.root = Objects.requireNonNull(rootList, "File is null or is not a directory");
    }

    public int size() {
        int i = 0;
        for (File file : this) {
            ++i;
        }
        return i;
    }

    @Override
    public Iterator<File> iterator() {
        return new FilenamesCrawlerIterator(root);
    }


    private static class FilenamesCrawlerIterator implements Iterator<File> {
        private final File[] files;
        private FilenamesCrawlerIterator child;
        private int i;

        public FilenamesCrawlerIterator(File[] files) {
            this.files = Objects.requireNonNull(files, "Files are null");
            this.child = null;
            this.i = 0;
            createChildIfNeeded();
        }

        @Override
        public boolean hasNext() {
            return i < files.length;
        }

        @Override
        public File next() {
            if (child != null && child.hasNext()) {
                File next = child.next();
                if (!child.hasNext()) {
                    child = null;
                    ++i;
                    createChildIfNeeded();
                }
                return next;
            }
            if (files[i].isFile()) {
                File file = files[i++];
                createChildIfNeeded();
                return file;
            }
            throw new NoSuchElementException();
        }

        private void createChildIfNeeded() {
            while (i < files.length && files[i].isDirectory()) {
                child = new FilenamesCrawlerIterator(files[i].listFiles());
                if (child.hasNext())
                    break;
                child = null;
                ++i;
            }
        }

    }
}
