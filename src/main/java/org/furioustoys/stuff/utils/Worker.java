package org.furioustoys.stuff.utils;

public abstract class Worker extends Thread
{
    protected boolean isWorking = false;
    protected boolean isStopped = false;

    protected void beforeWork(){}
    protected abstract void doIteration();
    protected void afterWork(){}

    public final void stopWorking()
    {
        isStopped = true;
        isWorking = false;
    }

    @Override
    public final void run()
    {
        try
        {
            while (true)
            {
                if (!isWorking)
                {
                    if (isStopped)
                    {
                        isStopped = false;
                        afterWork();
                    }
                    synchronized (this)
                    {
                        wait();
                    }
                    isWorking = true;
                    beforeWork();
                }
                else
                {
                    doIteration();
                }
            }
        }
        catch (InterruptedException ignored){}
    }
}
