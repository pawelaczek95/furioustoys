package org.furioustoys.stuff.utils.net;

import org.furioustoys.stuff.utils.JValidatableTextField;
import org.furioustoys.stuff.utils.ParametrizedDialog;

import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.GridLayout;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Scanner;

public class NetConfiguration {
    public final int port;

    public final String ip;
    public final boolean isClient;

    public NetConfiguration(int port, String ip, boolean isClient) {
        this.port = port;
        this.ip = ip;
        this.isClient = isClient;
    }

    public static NetConfiguration createFromUser() {
        boolean client;
        int port;
        String ip;
        System.out.println("Klient?");
        Scanner scanner = new Scanner(System.in);
        client = "t".equalsIgnoreCase(scanner.nextLine());
        System.out.println("Port: ");
        port = Integer.parseInt(scanner.nextLine());
        System.out.println("IP: ");
        ip = scanner.nextLine();
        return new NetConfiguration(port, ip, client);
    }

    public static NetConfiguration createFromGui() {
        return createFromGui(false);
    }
    public static NetConfiguration createFromGui(boolean forceClient) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("forceClient", forceClient);
        ParametrizedDialog.ReturnValue returnValue = ConfigurationDialog.openFirst(ConfigurationDialog.class, params, true);
        if (!returnValue.isAccepted)
            return null;
        return new NetConfiguration(Integer.parseInt((String) returnValue.parameters.get("PORT")), (String) returnValue.parameters.get("IP"), (Boolean) returnValue.parameters.get("CLIENT"));
    }

    @Override
    public String toString() {
        return "[" + ip + ":" + port + ", client: " + isClient + ")";
    }

    public static class ConfigurationDialog extends ParametrizedDialog {

        private JValidatableTextField ipTextField;
        private JValidatableTextField portTextField;
        private JCheckBox clientCheckbox;

        public ConfigurationDialog(JDialog owner, boolean isModal) {
            super(owner, isModal);
        }

        @Override
        protected JPanel createMainPanel(HashMap<String, Object> params) {
            setTitle("Konfiguracja");
            setResizable(false);
            JPanel contentPanel = new JPanel();
            contentPanel.setLayout(new GridLayout(5,1));
            contentPanel.add(new JLabel("Adres IP"));
            ipTextField = new JValidatableTextField() {
                @Override
                public boolean validateContent() {
                    try {
                        String text = getText();
                        if (text != null) {
                            Inet4Address.getByName(text);
                            return true;
                        }
                    } catch (UnknownHostException ignored) {
                    }
                    return false;
                }
            };
            ipTextField.setEnabled(false);
            ipTextField.setBackground(Color.GRAY);
            contentPanel.add(ipTextField);
            contentPanel.add(new JLabel("Port"));
            portTextField = new JValidatableTextField() {
                @Override
                public boolean validateContent() {
                    String text = getText();
                    if (text != null) {
                        try {
                            int port = Integer.parseInt(text);
                            return port >= 0 && port <= 65536;
                        } catch (NumberFormatException ignored) {
                        }
                    }
                    return false;
                }
            };
            contentPanel.add(portTextField);
            clientCheckbox = new JCheckBox("Czy klient?");
            clientCheckbox.addItemListener(itemEvent -> {
                boolean isSelected = clientCheckbox.isSelected();
                ipTextField.setEnabled(isSelected);
                ipTextField.setBackground(isSelected ? Color.WHITE : Color.GRAY);
            });
            contentPanel.add(clientCheckbox);
            if (params != null && ((Boolean) params.get("forceClient"))) {
                clientCheckbox.setSelected(true);
                clientCheckbox.setEnabled(false);
            }
            return contentPanel;
        }

        @Override
        protected boolean validateContent() {
            return ipTextField.validateContent() && portTextField.validateContent();
        }

        @Override
        protected HashMap<String, Object> returnParams() {
            HashMap<String, Object> toReturn = new HashMap<>();
            toReturn.put("IP", ipTextField.getText());
            toReturn.put("PORT", portTextField.getText());
            toReturn.put("CLIENT", clientCheckbox.isSelected());
            return toReturn;
        }
    }
}
