package org.furioustoys.stuff.utils.net;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketWrapper implements AutoCloseable {
    public final Socket socket;
    public final DataInputStream inputStream;
    public final DataOutputStream outputStream;

    private SocketWrapper(Socket socket, DataInputStream inputStream, DataOutputStream outputStream) {
        this.socket = socket;
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    @Override
    public void close() throws Exception {
        outputStream.close();
        inputStream.close();
        socket.close();
    }

    public static SocketWrapper of(String ip, int port) throws IOException {
        Socket socket = new Socket(ip, port);
        return createSocketWrapper(socket);
    }

    public static SocketWrapper of(ServerSocket serverSocket) throws IOException {
        Socket socket = serverSocket.accept();
        return createSocketWrapper(socket);
    }

    public static SocketWrapper of(NetConfiguration netConfiguration) throws IOException {
        return of(netConfiguration.ip, netConfiguration.port);
    }

    private static SocketWrapper createSocketWrapper(Socket socket) throws IOException {
        DataInputStream inputStream = null;
        DataOutputStream outputStream;
        try {
            inputStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            outputStream = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            return new SocketWrapper(socket, inputStream, outputStream);
        } catch (IOException e) {
            if (inputStream != null)
                inputStream.close();
            socket.close();
            throw e;
        }
    }
}
