package org.furioustoys.stuff.utils.encryption;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

public class EncryptionData {
    public final SecretKey key;
    public final IvParameterSpec iv;

    public EncryptionData(SecretKey key, IvParameterSpec iv) {
        this.key = key;
        this.iv = iv;
    }
}
