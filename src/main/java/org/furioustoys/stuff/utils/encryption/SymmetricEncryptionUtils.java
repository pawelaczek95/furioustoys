package org.furioustoys.stuff.utils.encryption;

import org.furioustoys.stuff.utils.net.SocketWrapper;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

public class SymmetricEncryptionUtils {

    public static final String DEFAULT_ALGO = "AES";
    public static final String DEFAULT_ALGO_VARIATION = "CBC";
    public static final String DEFAULT_PADDING = "PKCS5Padding";
    public static SecretKey generateAesKey(AES_SIZE size) throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(size.size);
        return keyGenerator.generateKey();
    }

    public static IvParameterSpec generateIv() {
        byte[] iv = new byte[16];
        new SecureRandom().nextBytes(iv);
        return new IvParameterSpec(iv);
    }

    public static int computePKCS5PaddingSize(int n) {
        if (n <= 0)
            throw new InvalidParameterException("Invalid array size: " + n);
        if (n % 16 == 0)
            return n;
        return n + 16 - (n % 16);
    }

    public static int padArrayPKCS5(byte[] array, int dataLength) {
        int paddedLength = computePKCS5PaddingSize(dataLength);
        if (paddedLength > array.length)
            throw new InvalidParameterException("Padded length bigger than array size: " + paddedLength + " > " + array.length);
        int paddingLen = paddedLength - dataLength;
        for (int i = dataLength; i < paddedLength; i++) {
            array[i] = (byte) paddingLen;
        }
        return paddingLen;
    }

    public static int computeDataLenWithoutPKCS5Padding(byte[] array, int len) throws BadPaddingException {
        int toReturn = len - array[len - 1];
        if (toReturn < 0)
            throw new BadPaddingException("Negative data length: " + toReturn);
        return toReturn;
    }

    public enum AES_SIZE {
        SIZE_128(128), SIZE_192(192), SIZE_256(256);

        public final int size;

        AES_SIZE(int size) {
            this.size = size;
        }

        public int getSize() {
            return size;
        }
    }

    public static EncryptionData sendSymmetricKey(SocketWrapper wrapper, SymmetricEncryptionUtils.AES_SIZE size) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
        SecretKey secretKey = SymmetricEncryptionUtils.generateAesKey(size);
        KeyFactory factory = KeyFactory.getInstance(AsymmetricEncryptionUtils.ALGORHITM);
        InputStream pubKeyStream = SymmetricEncryptionUtils.class.getClassLoader().getResourceAsStream("MyKeys_pub");
        if (pubKeyStream == null)
            throw new FileNotFoundException("Key file not found");
        PublicKey publicKey = AsymmetricEncryptionUtils.readPublicFromStream(factory, pubKeyStream);
        Cipher cipher = Cipher.getInstance(AsymmetricEncryptionUtils.ALGORHITM);
        cipher.init(Cipher.ENCRYPT_MODE,publicKey);

        byte[] encryptedKeyBytes = cipher.doFinal(secretKey.getEncoded());
        wrapper.outputStream.writeInt(encryptedKeyBytes.length);
        wrapper.outputStream.write(encryptedKeyBytes);

        IvParameterSpec ivParameterSpec = SymmetricEncryptionUtils.generateIv();
        byte[] encryptedIV = cipher.doFinal(ivParameterSpec.getIV());
        wrapper.outputStream.writeInt(encryptedIV.length);
        wrapper.outputStream.write(encryptedIV);
        wrapper.outputStream.flush();
        return new EncryptionData(secretKey, ivParameterSpec);
    }

    public static EncryptionData receiveSymmetricKey(SocketWrapper wrapper) throws IOException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeySpecException {
        int size = wrapper.inputStream.readInt();
        byte[] encryptedKey = new byte[size];
        wrapper.inputStream.readFully(encryptedKey);

        int ivSize = wrapper.inputStream.readInt();
        byte[] ivEncryptedBytes = new byte[ivSize];
        wrapper.inputStream.readFully(ivEncryptedBytes);
        KeyFactory factory = KeyFactory.getInstance(AsymmetricEncryptionUtils.ALGORHITM);
        PrivateKey privateKey = AsymmetricEncryptionUtils.readPrivateFromFile(factory, "MyKeys_priv");
        Cipher cipher = Cipher.getInstance(AsymmetricEncryptionUtils.ALGORHITM);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] keyBytes = cipher.doFinal(encryptedKey);
        byte[] ivBytes = cipher.doFinal(ivEncryptedBytes);

        return new EncryptionData(new SecretKeySpec(keyBytes, SymmetricEncryptionUtils.DEFAULT_ALGO), new IvParameterSpec(ivBytes));
    }

}
