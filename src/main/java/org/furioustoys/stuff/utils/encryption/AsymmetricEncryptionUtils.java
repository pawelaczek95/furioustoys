package org.furioustoys.stuff.utils.encryption;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class AsymmetricEncryptionUtils {
    public static final String ALGORHITM = "RSA";
    public static final String PRIV_SUFFIX = "_priv";
    public static final String PUB_SUFFIX = "_pub";

    public static KeyPair generatekeys(int size) throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance(ALGORHITM);
        generator.initialize(size);
        return generator.generateKeyPair();
    }

    public static void writeKeysToFiles(KeyPair keyPair, String nameTemplate) {
        File privFile = new File(nameTemplate + PRIV_SUFFIX);
        File pubFile = new File(nameTemplate + PUB_SUFFIX);
        privFile.delete();
        pubFile.delete();
        try (FileOutputStream priv = new FileOutputStream(privFile);
             FileOutputStream pub = new FileOutputStream(pubFile)) {
            priv.write(keyPair.getPrivate().getEncoded());
            pub.write(keyPair.getPublic().getEncoded());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static KeyPair readKeysFromFiles(String templateName) throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
        KeyFactory factory = KeyFactory.getInstance(ALGORHITM);
        return new KeyPair(readPublicFromFile(factory,templateName + PUB_SUFFIX), readPrivateFromFile(factory,templateName + PRIV_SUFFIX));
    }

    public static PublicKey readPublicFromFile(KeyFactory factory, String fileName) throws IOException, InvalidKeySpecException {
        byte[] pubBytes = Files.readAllBytes(Paths.get(fileName));
        X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(pubBytes);
        return factory.generatePublic(pubSpec);
    }

    public static PrivateKey readPrivateFromFile(KeyFactory factory, String fileName) throws IOException, InvalidKeySpecException {
        byte[] privBytes = Files.readAllBytes(Paths.get(fileName));
        PKCS8EncodedKeySpec privSpec = new PKCS8EncodedKeySpec(privBytes);
        return factory.generatePrivate(privSpec);
    }

    public static PublicKey readPublicFromStream(KeyFactory factory, InputStream inputStream) throws IOException, InvalidKeySpecException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(inputStream.available());
        while (inputStream.available() > 0)
            outputStream.write(inputStream.read());
        X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(outputStream.toByteArray());
        return factory.generatePublic(pubSpec);
    }

    public static PrivateKey readPrivateFromStream(KeyFactory factory, InputStream inputStream) throws IOException, InvalidKeySpecException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(inputStream.available());
        while (inputStream.available() > 0)
            outputStream.write(inputStream.read());
        X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(outputStream.toByteArray());
        return factory.generatePrivate(pubSpec);
    }
}
