package org.furioustoys.stuff.utils;

import java.io.File;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

public class Utils {

    private Utils(){}

    public static boolean isPrime(long n) {
        if (n < 2)
            return false;
        if (n == 2) {
            return true;
        }
        if (n % 2 == 0) {
            return false;
        }
        long s = (long) Math.sqrt(n);
        for (long i = 3; i <= s; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static Collection<File> getAllFilenames(File path){
        Collection<File> toReturn = new HashSet<>();
        File[] files = path.listFiles();
        if (files == null) {
            throw new RuntimeException("Path probably doesn't exist");
        }
        for (File file : files) {
            if (file.isFile()) {
                toReturn.add(file);
            } else if (file.isDirectory()) {
                toReturn.addAll(getAllFilenames(file));
            }
        }
        return toReturn;
    }

    public static String arrayToString(int[] array)
    {
        StringBuilder builder = new StringBuilder("[");
        for (int i=0; i<array.length-1; ++i)
            builder.append(array[i]).append(";");
        builder.append(array[array.length - 1]).append("]");
        return builder.toString();
    }

    public static String formatException(Throwable e) {
        return e.getClass().getCanonicalName() + ": " + e.getMessage();
    }

    public static Date leaveOnlyClockData(Date date, boolean deleteSeconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND,0);
        if (deleteSeconds)
            calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.DAY_OF_YEAR,0);
        calendar.set(Calendar.MONTH,0);
        calendar.set(Calendar.YEAR,0);
        return calendar.getTime();
    }

    public static String formatByteCount(long byteCount) {
        String[] suffixes = new String[]{"B", "KB", "MB", "GB"};
        StringBuilder builder = new StringBuilder();
        for (String suffix : suffixes) {
            builder.append(byteCount).append(suffix).append(" ");
            byteCount /= 1024;
        }
        return builder.toString().trim();
    }

    public static int indexOf(byte[] array, byte[] target, int start) {
        return indexOf(array, target, start, array.length);
    }
    public static int indexOf(byte[] array, byte[] target, int start, int end) {
        int targetPointer = 0;
        int limit = Math.min(array.length, end);
        for (int i = start; i < limit; i++) {
            if (array[i] == target[targetPointer]) {
                ++targetPointer;
                if (targetPointer >= target.length) {
                    return i - target.length + 1;
                }
            } else {
                if (targetPointer > 0)
                    --i;
                targetPointer = 0;
            }
        }
        return -1;
    }
}
