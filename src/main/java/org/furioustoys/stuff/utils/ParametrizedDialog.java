package org.furioustoys.stuff.utils;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class ParametrizedDialog extends JDialog {

    public static final String SIZE_PARAMETER = "SIZE_PARAMETER";

    protected boolean accepted;
    protected JButton acceptButton = null;
    protected JButton cancelButton = null;

    public ParametrizedDialog(JDialog owner, boolean modal) {
        super(owner, modal);
    }

    protected JPanel createMainPanel(HashMap<String, Object> params) {return new JPanel();}

    protected HashMap<String, Object> returnParams() {
        return null;
    }

    protected boolean validateContent() {return true;}

    public <T extends ParametrizedDialog> ReturnValue open(Class<T> frameClass, HashMap<String, Object> params) {
        return open(frameClass, params, false, this);
    }

    public <T extends ParametrizedDialog> ReturnValue open(Class<T> frameClass, HashMap<String, Object> params, boolean isConfirmable) {
        return open(frameClass, params, isConfirmable, this);
    }

    public static <T extends ParametrizedDialog> ReturnValue openFirst(Class<T> frameClass, HashMap<String, Object> params) {
        return openFirst(frameClass, params, false);
    }

    public static <T extends ParametrizedDialog> ReturnValue openFirst(Class<T> frameClass, HashMap<String, Object> params, boolean isConfirmable) {
        return open(frameClass, params, isConfirmable, null);
    }
    private static <T extends ParametrizedDialog> ReturnValue open(Class<T> frameClass, HashMap<String, Object> params, boolean isConfirmable, JDialog owner) {
        try {
            T frame = frameClass.getDeclaredConstructor(JDialog.class, boolean.class).newInstance(owner, true);
            frame.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
            JPanel mainPanel = frame.createMainPanel(params);
            frame.setLayout(new BorderLayout());
            frame.add(mainPanel, BorderLayout.CENTER);
            JPanel bottomPanel = new JPanel();
            bottomPanel.setLayout(new BorderLayout());
            frame.add(bottomPanel, BorderLayout.SOUTH);
            String cancelButtonTitle = isConfirmable ? "Anuluj" : "Zamknij";
            if (isConfirmable) {
                frame.acceptButton = new JButton("Zatwierdź");
                frame.acceptButton.addActionListener(event -> {
                    if (frame.validateContent()) {
                        frame.accepted = true;
                        frame.setVisible(false);
                    } else {
                        JOptionPane.showMessageDialog(frame, "Some things that you wrote are invalid", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                });
                bottomPanel.add(frame.acceptButton, BorderLayout.WEST);
            }
            frame.cancelButton = new JButton(cancelButtonTitle);
            frame.cancelButton.addActionListener(new CancellingActionListener(frame));
            bottomPanel.add(frame.cancelButton, BorderLayout.EAST);
            frame.pack();
            Object object = params.get(SIZE_PARAMETER);
            if (object instanceof Dimension)
                frame.setSize((Dimension) object);
            frame.setVisible(true);
            HashMap<String, Object> toReturn = frame.returnParams();
            frame.dispose();
            return new ReturnValue(frame.accepted, toReturn);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException("A mistake happened. Please report (the mistake ofc, not the programmer)", e);
        }
    }

    public static class CancellingActionListener implements ActionListener {

        private final ParametrizedDialog frame;

        public CancellingActionListener(ParametrizedDialog frame) {
            this.frame = frame;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            frame.accepted = false;
            frame.setVisible(false);
        }
    }


    public static class ReturnValue {
        public final boolean isAccepted;
        public final HashMap<String, Object> parameters;

        public ReturnValue(boolean isAccepted, HashMap<String, Object> parameters) {
            this.isAccepted = isAccepted;
            this.parameters = parameters;
        }
    }
}
