package org.furioustoys.stuff.utils;

import javax.swing.JTextField;
import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public abstract class JValidatableTextField extends JTextField {

    public JValidatableTextField() {
        addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                setBackground(validateContent() ? Color.WHITE : Color.RED);
            }
        });
    }

    public abstract boolean validateContent();
}
