package org.furioustoys.stuff;

import org.furioustoys.stuff.utils.FilenamesCrawler;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;

public class Main {

    public static final String BLACKSTAR_PATH = "/home/pawel/OBRAZKI/blackstar.jpg";
    public static final String SOUND_NAME = "/home/pawel/Sounds/blad.wav";
    private static boolean isWorking = false;

    public static void main(String[] args) throws Exception {
        System.out.println(Toolkit.getDefaultToolkit().getScreenSize());
        FilenamesCrawler crawler = new FilenamesCrawler("/home/pawel/Sounds/");
        System.out.println(crawler.size());
        System.exit(0);
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(
                new KonamiKeyboardFocusManager(SOUND_NAME, "/home/pawel/OBRAZKI/biohazard2.jpg"));
        ImageIcon imageIcon = new ImageIcon();
        BufferedImage image = ImageIO.read(new File(BLACKSTAR_PATH));
        imageIcon.setImage(image);
        JFrame frame = new JFrame("Testing furious toys");
        ImageTransformer.PixelizingWorker worker = new ImageTransformer.PixelizingWorker(frame,imageIcon,BLACKSTAR_PATH);
        frame.add(new JLabel(imageIcon));
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setIconImage(ImageIO.read(new File("/home/pawel/OBRAZKI/haker-bandery.jpg")));
        frame.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if (e.getKeyCode() == KeyEvent.VK_CONTROL)
                {
                    if (isWorking)
                    {
                        worker.stopWorking();
                        isWorking = false;
                        imageIcon.setImage(image);
                        frame.repaint();
                    }
                    else
                    {
                        isWorking = true;
                        synchronized (worker)
                        {
                            worker.notify();
                        }
                    }
                }
                else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    System.exit(0);
            }
        });
        worker.start();
    }
}
