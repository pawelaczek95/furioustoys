package org.furioustoys.stuff;

import org.furioustoys.stuff.utils.Worker;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageTransformer
{
    private ImageTransformer(){}

    public static void collapseColors(BufferedImage image)
    {
        for (int i = 0; i < image.getWidth(); ++i)
            for (int j = 0; j < image.getHeight(); ++j)
                image.setRGB(i, j, image.getRGB(i, j) & 0xFFC0C0C0);
    }

    public static void pixelize(BufferedImage image)
    {
        pixelize(image,1);
    }

    public static void pixelize(BufferedImage image, final int radius)
    {
        int counter = 0;
        int[] color = new int[4];
        for (int i = radius; i < image.getWidth(); i += (2*radius+1))
        {
            for (int j = radius; j < image.getHeight(); j += (2*radius+1))
            {
                for (int cnt = 0; cnt < 4; ++cnt)
                    color[cnt] = 0;
                counter = 0;
                for (int k = i - radius; k <= i + radius && k < image.getWidth(); ++k)
                {
                    for (int l = j - radius; l <= j + radius && l < image.getHeight(); ++l)
                    {
                        int[] myColor = transformRGBtoints(image.getRGB(k,l));
                        for (int cnt = 0; cnt < color.length; ++cnt)
                            color[cnt] += myColor[cnt];
                        ++counter;
                    }
                }
                for (int cnt = 0; cnt < color.length; ++cnt)
                    color[cnt] /= counter;
                int colorRGB = transformInstToRGB(color);
                for (int k = i - radius; k <= i + radius && k < image.getWidth(); ++k)
                    for (int l = j - radius; l <= j + radius && l < image.getHeight(); ++l)
                        image.setRGB(k,l,colorRGB);
            }
        }
    }

    public static int[] transformRGBtoints(int rgb) //rgba
    {
        int[] toReturn = new int[4];
        toReturn[2] = rgb & 0x000000FF;
        rgb >>= 8;
        toReturn[1] = rgb & 0x000000FF;
        rgb >>= 8;
        toReturn[0] = rgb & 0x000000FF;
        rgb >>= 8;
        toReturn[3] = rgb & 0x000000FF;
        return toReturn;
    }

    public static int transformInstToRGB(int[] ints)
    {
        if (ints.length != 4)
            throw new RuntimeException("Wrong number of colors");
        return (ints[3] << 24) + (ints[0] << 16) + (ints[1] << 8) + ints[2];
    }


    public static BufferedImage copyImage(BufferedImage source)
    {
        BufferedImage toReturn = new BufferedImage(source.getWidth(),source.getHeight(),source.getType());
        Graphics graphics = toReturn.getGraphics();
        graphics.drawImage(source,0,0,null);
        graphics.dispose();
        return toReturn;
    }

    public static class PixelizingWorker extends Worker
    {
        private final ImageIcon imageIcon;
        private final JFrame frame;
        private final BufferedImage[] frames;
        private static final long millisecondsPerFrame = 200;
        private int pointer;
        private int direction;
        private long currentTime;

        public PixelizingWorker(JFrame frame, ImageIcon imageIcon, String filepath) throws IOException
        {
            this.frame = frame;
            this.isWorking = false;
            this.imageIcon = imageIcon;
            this.frames = new BufferedImage[10];
            init(filepath);
        }

        private void init(String filepath) throws IOException
        {
            frames[0] = ImageIO.read(new File(filepath));
            for (int i=1; i<frames.length; ++i)
            {
                frames[i] = copyImage(frames[0]);
                pixelize(frames[i],i);
            }
        }

        public void setImage(String filepath) throws IOException
        {
            synchronized (this)
            {
                init(filepath);
            }
        }

        @Override
        protected void beforeWork()
        {
            pointer = 0;
            direction = 1;
            currentTime = System.currentTimeMillis();
        }

        @Override
        protected void doIteration()
        {
            if (System.currentTimeMillis() - currentTime > millisecondsPerFrame)
            {
                synchronized (this)
                {
                    currentTime = System.currentTimeMillis();
                    pointer += direction;
                    if (direction + pointer < 0 || direction + pointer >= frames.length)
                        direction = -direction;
                    imageIcon.setImage(frames[pointer]);
                    frame.repaint();
                }
            }
            else
            {
                try
                {
                    Thread.sleep(10);
                }
                catch (InterruptedException e)
                {
                    stopWorking();
                }
            }
        }
    }
}
